### beluca HTML Email Signature Generator

## How to use:

## Setup new company generator:

- Edit variables.php file with new details
- Edit signature-template.php with styling as required

## Do's and don't of HTML signatures
- Don't add styles in a separate/linked CSS file
- Do add styles inline or in a style tage in the 'signature-template.php' file
- Do check up on your HTML3 standards so you are across what is and isn't supported. At the time of writing email signatures are quite limited and not being aware of the limitations will yeild strange results.
- Don't expect externally linked fonts to always work. In fact they will probably rarely work.
- Don't add a BODY or HTML tag into the signature-template.php file. The email client will do this automatically and adding an additional one will screw it up.