<?php
function issetor(&$var, $default = false) {
   return isset($var) ? $var : $default;
}

function dd($input) {
  var_dump($input);
  die;
}

// These are helper functions specific to HTML email signatures
function width($width) {
  echo "width='$width' style='width: " . $width . "px';";
}

function height($height) {
  echo "height='$height' style='height: " . $height ."px';";
}

function padding($side, $amount) {
  $array["top"] = 0;
  $array["right"] = 0;
  $array["bottom"] = 0;
  $array["left"] = 0;

  $array[$side] = $amount;

  echo "style='padding: " . $array["top"] . "px " . $array["right"] . "px " . $array["bottom"] . "px " . $array["left"] . "px;'";
}

function margin($side, $amount) {
  $array["top"] = 0;
  $array["right"] = 0;
  $array["bottom"] = 0;
  $array["left"] = 0;

  $array[$side] = $amount;

  echo "style='margin: " . $array["top"] . "px " . $array["right"] . "px " . $array["bottom"] . "px " . $array["left"] . "px;'";
}
?>
