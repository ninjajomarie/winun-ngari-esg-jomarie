<?php
	include_once "variables.php";

  session_start(['read_and_close'  => true]);

  $downloadfile = $_POST['outfile'];
  $name = $_POST['name'];

  if(empty($name))
  {
    echo "You must enter a name!<br/>";
    echo "<button>Return</button>";
    die;
  }

  header("Content-type: text/plain");
  header("Content-Disposition: attachment; filename=" . $businessName . " - " . $name .".htm");

  echo $downloadfile;
?>
