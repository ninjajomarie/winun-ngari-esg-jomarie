<?php
	include_once "variables.php";
?>

<?php
  $name = $_POST['name'];
  $job = $_POST['job'];
  $email = $_POST['email'];
	$mobile = $_POST['mobile'];
  // $mobile = $_POST['mobile'];
  $phone = isset($_POST['phone']) ?? '';
  $address = $_POST['address'];
  $image = $_POST['image'];
?>

<style type="text/css">
  @media screen {
    @font-face {
      font-family: 'Calibri';
      font-style: normal;
      font-weight: 400;
      src: local('Calibri Regular'), local('Calibri'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
    }
  }

  body {
    font-family: "Calibri", "Lucida Grande", "Lucida Sans Unicode", Tahoma, Sans-Serif;
  }
</style>
<table width="<?= $signatureWidth ?>" cellpadding="0" cellspacing="0" border="0">
	<!--Row 1  -->
	<tr>
		<td valign="top">
			<?= $logo[$image]->display(220) ?>
		</td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
				<td>
					<?php if(!empty($name)){ ?>
					<!-- Name (Only displays if the variable contains data.) -->
					<tr>
						<td style="<?= $styHead1 ?>">
						<?php echo $name; ?>
						</td>
					</tr>

					<?php }; ?>
				</td>
				</tr>
				<tr>
				<td>
					<?php if(!empty($job)){ ?>
					<!-- Job (Only displays if the variable contains data.) -->
					<tr>
						<td style="<?= $styHead3 ?>">
						<?php echo $job; ?>
						</td>
					</tr>
					<tr <?= height(10) ?>>
						<td></td>
					</tr>
					<?php }; ?>
				</td>
				</tr>
				<!-- Phone -->
				<tr>
					<td style="<?= $styHead2 ?>">
						<b style="<?= $secondaryColor ?>;">P</b> 08 9172 5525
					</td>
				</tr>
				<!-- Mobile -->
				<?php if(!empty($mobile)){ ?>
				<tr>
					<td style="<?= $styHead2 ?>">
					<b style="<? $secondaryColor ?>;">M</b> <?php echo $mobile; ?>
					</td>
				</tr>
				<?php }; ?>

				<!-- Email -->
				<?php if(!empty($email)){ ?>
				<tr>
					<td style="<?= $styHead2 ?>">
					<b style="<? $secondaryColor ?>;">E</b> <?php echo $email; ?>
					</td>
				</tr>
				<?php }; ?>

				<!-- Url -->
				<?php if(!empty($URL)){ ?>
				<tr>
					<td style="<?= $styHead2 ?>">
					<b style="<? $secondaryColor ?>;">W</b>  <?php echo $URL; ?>
					</td>
				</tr>
				<?php }; ?>

						<!-- Address -->
				<?php if(!empty($address)){ ?>
					<tr>
						<td style="<?= $styHead2 ?>">
							<b style="<? $secondaryColor ?>">A</b>  <?php echo $address; ?>
						</td>
					</tr>
				<?php }; ?>
			</table>
		</td>
	</tr>
	<!--Row 2  -->
	<tr>
		<td valign="top" colspan="3">
		<table>
			<tr >
			<td td colspan="2" style="<?= $styTagline ?>">
				<a href="https://<?= $URL ?>">
				<?= $images[$image]->display(600) ?>
				</a>
			</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr <?= height(10) ?>>
		<td></td>
	</tr>
	<tr>
		<!-- Disclaimer -->
		<td colspan="4" >
			<p style="<?= $styDisclaimer ?>"><?= $disclaimer["content"] ?></p>
		</td>
	</tr>
</table>
