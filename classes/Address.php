<?php
class address
{
    // Properties
    public $locationName;
    public $line1;
    public $line2;
    public $suburb;
    public $state;
    public $postcode;

    public function printAddressInline() {
        return issetor($this->line1) .
        issetor($this->line2) .
        issetor($this->suburb) . issetor($this->state) . issetor($this->postcode);
    }

    public function printAddressBlock() {
        echo (!empty($this->line1) ? $this->line1 . "<br>" : "") .
        (!empty($this->line2) ? $this->line2 . "<br>" : "") .
        issetor($this->suburb) . issetor($this->state) . issetor($this->postcode);
    }

    public function printAddressLocation() {
        echo issetor($this->locationName);
    }
}
